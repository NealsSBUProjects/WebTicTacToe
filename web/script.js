/*jslint browser: true*/
/*global $, jQuery, alert, HTMLCanvasElement, console, Materialize*/

var gameStart = false;
var tictactoeboard;

$(document).ready(function () {
    "use strict";
    drawO($("#humanType").get(0).getContext('2d'));
    drawX($("#computerType").get(0).getContext('2d'));


    $("#board").children().each(function () {
        if (this instanceof HTMLCanvasElement) {
            console.log("Hi");
        }
    });

    $.post("neither", function( data ){
        var json = JSON.parse(data);
        refreshPlayers(json);
    });

});

function drawX(ctx) {
    "use strict";
    ctx.lineWidth = 5;
    ctx.strokeStyle = "#004d40";
    ctx.lineCap = 'round';

    ctx.beginPath();
    ctx.moveTo(5, 5);
    ctx.lineTo(ctx.canvas.width - 5, ctx.canvas.height - 5);

    ctx.stroke();
    ctx.moveTo(ctx.canvas.width - 5, 5);
    ctx.lineTo(5, ctx.canvas.height - 5);
    ctx.stroke();
}

function drawO(ctx) {
    "use strict";
    ctx.lineWidth = 5;

    ctx.strokeStyle = "#004d40";
    ctx.beginPath();
    ctx.arc(50, 50, 45, 0, 2 * Math.PI);
    ctx.stroke();
}

function setPlayerNameByNum( i, name ) {
    document.getElementById("player".concat(i)).firstChild.textContent = name.substring(0,23);
}

function setScoreByNum(i, score) {
    document.getElementById("score".concat(i)).firstChild.textContent = score;
}

function refreshPlayers( json ) {

    var leaders = $(".collection").children;
    for(var i = 0; i < json.players.length; i++){

        setPlayerNameByNum(i, json.players[i].name);

        setScoreByNum(i, json.players[i].score);
    }

}

function getCanvasByNum(canvasNum) {
    "use strict";
    return document.getElementById("canvas".concat(canvasNum)).getContext("2d");
}

var TieException= {};
function tie() {
    var t = tictactoeboard;
    if(t[0] != 'e' && t[1] != 'e' && t[2] != 'e'
    && t[3] != 'e' && t[4] != 'e' && t[5] != 'e'
    && t[6] != 'e' && t[7] != 'e' && t[8] != 'e') {
        return true;
    }else{
        return false;
    }
}

function checkForWin(symbol) {
    "use strict";

    var wins = [ [0,1,2], [3,4,5], [6,7,8], [0,3,6],
                 [1,4,7], [2,5,8], [0,4,8], [2,4,6] ];

    var player;

    if( symbol === 'x' ){
        canPlayerClick = true;
        player = "Computer";
    }else{
        player = "Human";
    }

    try {
        wins.forEach(function (element, index, array) {
            if (tictactoeboard[element[0]] === symbol
                && tictactoeboard[element[1]] === symbol
                && tictactoeboard[element[2]] === symbol) {
                $("#result").text(player + " Won! Press Start to play again.");
                gameStart = false;
                computerGoesFirst = !computerGoesFirst;
                if (symbol === 'x') {
                    $.post("loss", function( data ){
                        var json = JSON.parse(data);
                        refreshPlayers(json);
                        if(json.hasOwnProperty("you")) {
                            $("#result").text(player + " Won! Press Start to play again.\n"
                                + "You have " + json.you + " points!");
                        }
                    });
                } else {
                    $.post("win", function( data ){
                        var json = JSON.parse(data);
                        refreshPlayers(json);
                        if(json.hasOwnProperty("you")) {
                            $("#result").text(player + " Won! Press Start to play again.\n"
                                + "You have " + json.you + " points!");
                        }
                    });
                }
            }
            if (tie()) {
                throw TieException;
            }
        });
    } catch (e){
        if(e!=TieException) throw e;
    }
}

function computerMove() {

    var hasntMoved = true;

    if(!gameStart){
        return;
    }

    while (hasntMoved) {
        hasntMoved = !tie();
        var guess = Math.round(Math.random() * 8);
        if (tictactoeboard[guess] === 'e') {
            tictactoeboard[guess] = 'x';
            drawX(getCanvasByNum(guess));
            hasntMoved = false;
        }
    }
}

var canPlayerClick = true;
function canvasClicked(canvasNum) {
    "use strict";

    if (!gameStart){
        return;
    }
    if(!canPlayerClick) {
        return;
    }

    checkForWin('x');

    if(tictactoeboard[canvasNum] === 'x'){
        return;
    }
    if(tictactoeboard[canvasNum] === 'o'){
        return;
    }

    if (tictactoeboard[canvasNum] === 'e') {
        tictactoeboard[canvasNum] = 'o';
        drawO(getCanvasByNum(canvasNum));
        canPlayerClick = false;
    }

    checkForWin('o');

    computerMove();


    checkForWin('x');

    if(tie()){
        $("#result").text("Draw!");
    }


}

function clearAll() {
    "use strict";
    var i;
    for (i = 0; i < 9; i += 1) {
        getCanvasByNum(i).clearRect(0, 0,
            getCanvasByNum(i).canvas.width, getCanvasByNum(i).canvas.height);
    }
    $("#result").text("Play!");
}

function startGameCountDown(){
    var i = 5;
    var inter = setInterval(function(){
        Materialize.toast(i, 500);
        i--;
        if(i == -1){
            Materialize.toast("Go!", 400);
            clearInterval(inter);
            startGame();
        }
    }, 1000);

}

var computerGoesFirst = false;

function startGame() {
    "use strict";
    gameStart = true;

    clearAll();

    tictactoeboard = [ 'e', 'e', 'e',
                       'e', 'e', 'e',
                       'e', 'e', 'e' ];

    if(computerGoesFirst){
        computerMove();
    }

}

