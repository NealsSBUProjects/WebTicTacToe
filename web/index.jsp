<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
<div class="container">
    <div class="row">
        <h2>Neal's Tic Tac Toe</h2>
    </div>
    <div class="row">
        <div class="col s2" id="instructions">
            <h5>Human:</h5>
            <canvas class="type" width="100" height="100" id="humanType"></canvas>
            <h5>Computer:</h5>
            <canvas class="type" width="100" height="100" id="computerType"></canvas>
        </div>
        <div class="col s5" id="board">
            <div class="row">
                <canvas class="square" width="100" height="100" id="canvas0" onclick="canvasClicked(0)"></canvas>
                <canvas class="square" width="100" height="100" id="canvas1" onclick="canvasClicked(1)"></canvas>
                <canvas class="square" width="100" height="100" id="canvas2" onclick="canvasClicked(2)"></canvas>
            </div>
            <div class="row">
                <canvas class="square" width="100" height="100" id="canvas3" onclick="canvasClicked(3)"></canvas>
                <canvas class="square" width="100" height="100" id="canvas4" onclick="canvasClicked(4)"></canvas>
                <canvas class="square" width="100" height="100" id="canvas5" onclick="canvasClicked(5)"></canvas>
            </div>
            <div class="row">
                <canvas class="square" width="100" height="100" id="canvas6" onclick="canvasClicked(6)"></canvas>
                <canvas class="square" width="100" height="100" id="canvas7" onclick="canvasClicked(7)"></canvas>
                <canvas class="square" width="100" height="100" id="canvas8" onclick="canvasClicked(8)"></canvas>
            </div>
            <div class="row">
                <p id="result"></p>
            </div>
        </div>
        <div class="col s3" id="leadingPlayers">
            <div class="row">
                <div class="collection">
                    <p id="player0" class="collection-item">Nil<span id="score0" class="badge">Filler</span></p>
                    <p id="player1" class="collection-item">Antoine<span id="score1" class="badge">Filler</span></p>
                    <p id="player2" class="collection-item">PhilCheese<span id="score2" class="badge">Filler</span></p>
                    <p id="player3" class="collection-item">Gustabo<span id="score3" class="badge">Filler</span></p>
                </div>
            </div>
            <div class="row">
                <div class="col s2 controls" id="login">
                </div>

                <c:choose>
                    <c:when test="${user != null}">
                        <div class="card white">
                            <div class="card-content white darken-4-text">
                                <span class="card-title">Welcome Back!</span>
                                <p>Keep the robot apocalypse at bay! Prove your superior human intelligence!</p>
                            </div>
                            <div class="card-action">
                                <a id="startLink" onclick="startGameCountDown()">Start</a>
                                <a id="loginLink" href="${logoutUrl}">Logout</a>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>

                        <div class="card white">
                            <div class="card-content white darken-4-text">
                                <span class="card-title">Welcome!</span>
                                <p>Log in and rise to the top!</p>
                            </div>
                            <div class="card-action">
                                <a id="startLink" onclick="startGameCountDown()">Start</a>
                                <a id="loginLink" href="${loginUrl}">Login</a>
                            </div>
                        </div>

                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>