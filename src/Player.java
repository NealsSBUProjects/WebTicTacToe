import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by neal on 2/13/16.
 */
@Entity
public class Player {

    @Id
    private String name;
    @Index
    private Integer score;

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player(String name, Integer score) {
        this.name = name;
        this.score = score;
    }

    public Player() {
    }
}
