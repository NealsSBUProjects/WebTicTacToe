import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;


/**
 * Created by neal on 2/9/16.
 */
public class LoginServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        String loginUrl = userService.createLoginURL("/");
        String logoutUrl = userService.createLogoutURL("/");

        if ( user != null ) {
            Player p = ObjectifyService.ofy().load()
                    .type(Player.class)
                    .id(user.getNickname()).now();

            if( p == null ){
                ObjectifyService.ofy().save().entity(new Player(user.getNickname(), 0)).now();
            }
        }

        req.setAttribute("user", user);
        req.setAttribute("loginUrl", loginUrl);
        req.setAttribute("logoutUrl", logoutUrl);

        resp.setContentType("text/html");

        RequestDispatcher jsp = req.getRequestDispatcher("/index.jsp");
        jsp.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();

        if( user != null ){ // Update score
            Player p = ObjectifyService.ofy().load()
                    .type(Player.class)
                    .id(user.getNickname()).now();
            if( p != null ){
                switch (req.getRequestURI()){
                    case "/win":
                        p.setScore(p.getScore() + 1);
                        break;
                    case "/lose":
                        p.setScore(p.getScore() - 1);
                        break;
                }
            }

            ObjectifyService.ofy().save().entity(p).now();
        }

        JsonObject json;
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();


        List<Player> top4 = ObjectifyService.ofy()
                .load()
                .type(Player.class)
                .order("score")
                .limit(4)
                .list();

        Collections.sort(top4, new PlayerComparator());

        for(Player p : top4){
            arrayBuilder.add(makePlayerJson(p));
        }

        JsonArray playersAsJson = arrayBuilder.build();


        if(user != null){
            Player p = ObjectifyService.ofy().load()
                    .type(Player.class)
                    .id(user.getNickname()).now();
            json = Json.createObjectBuilder()
                    .add("players", playersAsJson)
                    .add("you", p.getScore())
                    .build();
        }else{
            json = Json.createObjectBuilder()
                    .add("players", playersAsJson)
                    .build();
        }

        resp.getWriter().println(json);

    }

    private JsonObject makePlayerJson(Player p) {

        return Json.createObjectBuilder()
                .add("name", p.getName())
                .add("score", p.getScore())
                .build();

    }
}
