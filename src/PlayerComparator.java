import java.util.Comparator;

/**
 * Created by neal on 2/13/16.
 */
public class PlayerComparator implements Comparator<Player> {

    @Override
    public int compare(Player o1, Player o2) {

        return o2.getScore().compareTo(o1.getScore());

    }
}
